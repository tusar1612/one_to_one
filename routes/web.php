<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/profiles', function () {
   $teachers=\App\Teacher::find(2)->profile;

    return $teachers;
});

Route::get('/teachers', function () {
    $teachers=\App\Profile::find(2)->teacher;

    return $teachers;
});

